package com.example.challenge5.data.di

import com.example.challenge5.data.MovieDataSource
import com.example.challenge5.data.MovieRepository

object Injection {

    fun providerRepository(): MovieDataSource {
        return MovieRepository()
    }
}