package com.example.challenge5.data.preferences

class Constant {

    companion object {
        const val IS_LOGIN = "logged_in"
        const val EMAIL_USER = "email"
    }
}