package com.example.challenge5.data

interface MovieDataSource {

    fun retrieveMovie(callback: OperationCallback)
    fun retrieveDetailMovie(callback: OperationCallback, id_movie: String)
    fun cancel()

}